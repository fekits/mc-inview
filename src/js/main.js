import '../css/main.scss';

// 代码着色插件
import McTinting from '@fekit/mc-tinting';

new McTinting({
  theme: 'chrome',
});

// 楼层埋点插件
import McInview from '../../lib/mc-inview';

// 楼层埋点
const myInView = new McInview({
  el: '.am-view',
  theme: 'ad',
  on: {
    view(dom) {
      console.log(dom, '这个元素滚动到可视区了');
    },
  },
});

setTimeout(() => {
  document.body.innerHTML += `<div class="floor am-view">F11</div>
  <div class="floor am-view">F12</div>
  <div class="floor am-view">F13</div>
  <div class="floor am-view">F14</div>
  <div class="floor am-view">F15</div>
  <div class="floor am-view">F16</div>
  <div class="floor am-view">F17</div>
  <div class="floor am-view">F18</div>
  <div class="floor am-view">F19</div>`;
  myInView.refresh();
}, 5000);
