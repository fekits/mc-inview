# MC-INVIEW

```$xslt
页面滚动时监听指定元素滚动到可视区域时播放动画
```

### 索引

- [演示](#演示)
- [参数](#参数)
- [示例](#示例)
- [主题](#主题)
- [版本](#版本)
- [反馈](#反馈)

### 演示

电脑：[https://fekit.cn/plugins/mc-inview](https://fekit.cn/devices/#page=home&phone=2&browser=0&group=0&guide=0&url=http%3A%2F%2Ffekit.cn%2Fplugins%2Fmc-inview%2F)

手机：[https://fekit.cn/plugins/mc-inview](https://fekit.cn/plugins/mc-inview)

### 开始

NPM

```npm
npm i @fekit/mc-inview
```

CDN

```html
<script type="text/javascript" src="https://cdn.fekit.cn/@fekit/mc-inview/mc-inview.umd.min.js"></script>
```

### 参数

```$xslt
el      {String}   选择器
theme   {String}   插件配套的动画主题
offset  {Number}   元素触发动画时的偏移量 0-1
once    {Number}   是否仅播放一次动画
on:{
    view  {Function}  显示时触发
    none  {Function}  隐去时触发
}
```

### 示例

JS

```javascript
import McInview from '@fekit/mc-inview';

// 为所有class为am-view的元素添加进入可视区状态监听
const myInView = new McInview({
  el: '.am-view',
  theme: 'ad',
  on: {
    view(dom) {
      console.log(dom, '这个元素滚动到可视区了');
    },
  },
});

// 页面更新内容时刷新插件
setTimeout(() => {
  document.body.innerHTML += `<div class="floor am-view">F11</div>
  <div class="floor am-view">F12</div>
  <div class="floor am-view">F13</div>
  <div class="floor am-view">F14</div>
  <div class="floor am-view">F15</div>
  <div class="floor am-view">F16</div>
  <div class="floor am-view">F17</div>
  <div class="floor am-view">F18</div>
  <div class="floor am-view">F19</div>`;
  myInView.refresh();
}, 5000);
```

SCSS

```scss
// 载入动画主题
@import '@fekit/mc-view/theme/mc-inview@theme=aa';
@import '@fekit/mc-view/theme/mc-inview@theme=ab';
@import '@fekit/mc-view/theme/mc-inview@theme=ac';
@import '@fekit/mc-view/theme/mc-inview@theme=ad';
@import '@fekit/mc-view/theme/mc-inview@theme=ae';
```

### 主题
目前可用的主题有：
- mc-inview@theme=aa
- mc-inview@theme=ab
- mc-inview@theme=ac
- mc-inview@theme=ad
- mc-inview@theme=ae

### 版本

```$xslt
v1.0.9
1. 新增 refresh() 方法刷新，用于页面有异步加载的内容时刷新插件。
```

```$xslt
v1.0.6
1. 新增3款主题
2. 修改HTML可自定义主题
```

```$xslt
v1.0.3
1. 新增一款主题mc-inview@theme=ac.scss
```

```$xslt
v1.0.2
1. 重构核心代码，优化性能
```

```$xslt
v1.0.1
1. 此版本精简了代码，取消了onshow和onhide，回调合并为一个then，可以通赤then回调的传参来判断是显示还是消失及其它一些操作。
```

### 反馈

```$xslt
如果您在使用中遇到问题，请通过以下方式联系我。
QQ: 860065202
EMAIL: xiaojunbo@126.com
```
