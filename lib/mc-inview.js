// 兼容处理
const requestAnimFrame = (function () {
  return (
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    function (callback) {
      setTimeout(callback, 1000 / 60);
    }
  );
})();

let McInview = function (param) {
  this.param = param || {};
  this.refresh(param);
  window.addEventListener('scroll', () => {
    requestAnimFrame(() => {
      if (this.once) {
        if (this.tacking && this.tacked < this.size) {
          this.tacking = 0;
          this.core();
        } else {
          return false;
        }
      } else {
        if (this.tacking) {
          this.tacking = 0;
          this.core();
        } else {
          return false;
        }
      }
    });
  });
};
// 刷新
McInview.prototype.refresh = function (param) {
  this.param = Object.assign(this.param, param);
  const { el, once, theme, offset, on } = this.param;
  this.doms = document.querySelectorAll(el || '.mc-inview') || [];
  this.size = this.doms.length;
  this.once = once;
  this.theme = theme || 'aa';
  this.offset = window.innerHeight * (offset || 1);
  this.on = Object.assign({ view: () => {}, none: () => {} }, on || {});

  // 添加标识
  this.doms.forEach((dom) => {
    if (!dom.getAttribute('data-theme')) {
      dom.setAttribute('data-theme', this.theme);
    }
    dom.setAttribute('data-inview', 'none');
  });

  // 任务排队
  this.tacking = 1;
  this.tacked = 0;

  // 核心代码
  this.core();
};
// 核心
McInview.prototype.core = function () {
  if (this.once) {
    this.tacked = 0;
  }
  for (let i = 0; i < this.size; i++) {
    let dom = this.doms[i];
    if (this.once) {
      this.tacked = this.tacked + (dom._inviewStatus_ ? 1 : 0);
    }
    ((dom) => {
      let domRect = dom.getBoundingClientRect();
      if (domRect.top < this.offset) {
        if (!dom._inviewStatus_) {
          dom.setAttribute('data-inview', 'view');
          this.on.view(dom);
          dom._inviewStatus_ = 1;
        }
      } else {
        if (!this.once) {
          if (dom._inviewStatus_) {
            dom.setAttribute('data-inview', 'none');
            this.on.none(dom);
            dom._inviewStatus_ = 0;
          }
        }
      }
    })(dom);
  }
  this.tacking = 1;
};

export default McInview;
